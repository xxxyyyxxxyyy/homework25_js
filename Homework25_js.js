let prevVal = "";
let newVal = "";
let resultVal = "";
 let mathOperator = "";
 let decimalClicked = false;
 let valMemStored = "";
 function numButPress(num){
    if(resultVal){
        newVal = num;
        resultVal = "";
    } else {
        if(num === '.'){
            if(decimalClicked != true){
                newVal += num;
                decimalClicked = true;
            }
        } else {
            newVal += num;
        }
    }
        
    document.getElementById("result").value = newVal;
}
 
function mathButPress(operator){
    if(!resultVal){
        prevVal = newVal;
    } else {
 
        prevVal = resultVal;
    }
    
    newVal = "";
    decimalClicked = false;
    mathOperator = operator;
 
    resultVal = "";
    document.getElementById("result").value = "";
}
 
function equalButPress(){
    decimalClicked = false;
 
    prevVal = parseFloat(prevVal);
    newVal = parseFloat(newVal);
 
    switch(mathOperator){
        case "+":
            resultVal = prevVal + newVal;
            break;
        case "-":
            resultVal = prevVal - newVal;
            break;
        case "*":
            resultVal = prevVal * newVal;
            break;
        case "/":
            resultVal = prevVal / newVal;
            break;
            case 'm-':
            resultVal = 'M';
            break;
        default:
            resultVal = newVal;
    }
 
    prevVal = resultVal;
 
    document.getElementById("result").value = resultVal;
}
 
function clearButPress(){
    prevVal = "";
    newVal = "";
    resultVal = "";
    mathOperator = "";
    decimalClicked = false;
    document.getElementById("result").value = "0";
}


function copyButPress(){
    valMemStored = document.getElementById("result").value;
    
}
 
function pasteButPress(){
    if(valMemStored){
        document.getElementById("result").value = valMemStored;
        newVal = valMemStored;
    }
     
}

const mMinus = document.getElementById('m-');
const mPlus = document.getElementById("m+");

mMinus.addEventListener('click', () => document.getElementById("result").style.backgroundColor = '#ff0000');
mPlus.addEventListener('click', () => document.getElementById("result").style.backgroundColor = '#ff0050');
